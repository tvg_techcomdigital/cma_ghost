import unittest
from app.presenters.commercial_items_report_presenter import (
    CommercialItemsReportPresenter)
from app.models.commercial_item import CommercialItem
from app.models.errors import Error
from datetime import datetime


class TestCommercialItemsReportPresenter(unittest.TestCase):
    def test_empty_commercial_items_return_empty(self):
        presenter = CommercialItemsReportPresenter([], datetime.utcnow())
        self.assertFalse(any(presenter.generate_report().values()))

    def test_non_empty_commercial_items_return_correctly_classified(self):
        error = Error(description='ad unit not found',
                      data={'hierarchy_name': 'g1'})

        commercial_item_1 = CommercialItem()
        ref_datetime = datetime.utcnow()
        commercial_item_1.modified = ref_datetime
        commercial_item_2 = CommercialItem()

        presenter = CommercialItemsReportPresenter(
            [error, commercial_item_1, commercial_item_2], ref_datetime)

        self.assertTrue(all(presenter.generate_report().values()))
