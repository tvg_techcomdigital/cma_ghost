import json
import unittest
from app.models.errors import Error
from app.models.commercial_item import CommercialItem
from app.presenters.commercial_items_json_presenter import (
    CommercialItemsJSONPresenter)


class TestCommercialItemsJSONPresenter(unittest.TestCase):
    def setUp(self):
        error = Error(description='ad unit not found',
                      data={'hierarchy_name': 'g1'})
        commercial_item = CommercialItem()

        self.generate_json = CommercialItemsJSONPresenter(
            [commercial_item, error]).generate_json

    def test_generated_json_has_type_str(self):
        self.assertEqual(type(self.generate_json()), str)

    def test_generated_json_is_valid(self):
        try:
            self.assertEqual(type(json.loads(self.generate_json())), list)
        except TypeError:
            self.fail('Invalid JSON from CommercialItemsJSONPresenter')
