import json
import unittest
from app.models.structure_item import StructureItem
from app.models.ad_unit_data import AdUnitData
from app.factories.commercial_item_factory import CommercialItemFactory
from dotenv import load_dotenv
from os.path import abspath, dirname, join


class TestCommercialItemFactory(unittest.TestCase):
    def setUp(self):
        load_dotenv('.env.test', override=True)

        directory = '../fixtures/factories/commercial_item_factory'

        structure_item_data_file = join(dirname(abspath(__file__)),
                                        '%s/structure_item.json' % directory)

        ad_unit_data_file = join(dirname(abspath(__file__)),
                                 '%s/ad_unit_data.json' % directory)

        with open(structure_item_data_file) as f:
            structure_item = StructureItem(**json.load(f))

        with open(ad_unit_data_file) as f:
            ad_unit_data = AdUnitData(**json.load(f))

        self.commercial_item = CommercialItemFactory(structure_item,
                                                     ad_unit_data).create()

    def test_hierarchy_name_is_not_none(self):
        self.assertIsNotNone(self.commercial_item.hierarchy_name)

    def test_ad_unit_is_not_none(self):
        self.assertIsNotNone(self.commercial_item.ad_unit)

    def test_complemento_ad_unit_is_not_none(self):
        self.assertIsNotNone(self.commercial_item.complemento_ad_unit)

    def test_tenantId_is_not_none(self):
        self.assertIsNotNone(self.commercial_item.tenantId)

    def test_category_is_not_none(self):
        self.assertIsNotNone(self.commercial_item.category)

    def test_tag_manager_is_not_none(self):
        self.assertIsNotNone(self.commercial_item.tag_manager)

    def test_ad_custom_data_is_not_none(self):
        self.assertIsNotNone(self.commercial_item.ad_custom_data)

    def test_ad_account_id_is_not_none(self):
        self.assertIsNotNone(self.commercial_item.ad_account_id)

    def test_ad_cms_id_is_not_none(self):
        self.assertIsNotNone(self.commercial_item.ad_cms_id)
