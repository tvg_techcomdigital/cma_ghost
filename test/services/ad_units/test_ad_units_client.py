import unittest
from unittest.mock import patch
from app.utils import ad_units_client_config
from app.services.ad_units.ad_units_client import AdUnitsClient


class TestAdUnitsClient(unittest.TestCase):
    def setUp(self):
        self.discovery = patch(
            'app.services.ad_units.ad_units_client.discovery')
        self.discovery_mock = self.discovery.start()
        self.ad_units_client = AdUnitsClient(**ad_units_client_config())

        base_chain = self.discovery_mock.build().spreadsheets().values()

        self.get_chain = base_chain.get().execute
        self.get_chain.return_value = {'values': [['g1', 'tvg_G1']]}

        self.append_chain = base_chain.append().execute

    def tearDown(self):
        self.discovery.stop()

    def test_get_ad_units_calls_chain(self):
        self.ad_units_client.get_ad_units('g1')

        self.assertTrue(self.get_chain.called)

    def test_get_ad_units_returns_dict(self):
        self.assertEqual(type(self.ad_units_client.get_ad_units('g1')), dict)

    def test_create_ad_unit_calls_chain(self):
        self.ad_units_client.create_ad_unit('g1', {'hierarchy_name': 'g1'})

        self.assertTrue(self.append_chain.called)
