import unittest
from unittest.mock import MagicMock
from app.models.errors import AdUnitNotFoundError, AdUnitIgnoredError
from app.models.ad_unit_data import AdUnitData
from app.services.ad_units.ad_unit_data_fetcher import AdUnitDataFetcher


class TestAdUnitDataFetcher(unittest.TestCase):
    def setUp(self):
        raw_ad_units = {
            'g1': {
                'ad_unit': 'tvg_G1.Home', 'status': 'CLASSIFICADO'},
            'g1/economia': {
                'ad_unit': 'tvg_G1/Economia', 'status': 'PENDENTE'},
            'g1/agronegócio': {
                'ad_unit': '', 'status': 'CLASSIFICADO'},
            'g1/globonews': {
                'ad_unit': '', 'status': 'IGNORADO'}}

        self.ad_units_client = MagicMock()
        self.ad_units_client.get_ad_units.return_value = raw_ad_units

        self.get_ad_unit_data = AdUnitDataFetcher(self.ad_units_client)\
            .get_ad_unit_data
        self.tenant = 'g1'

    def test_ad_unit_is_returned_when_found_and_classified(self):
        hierarchy_name = 'g1'

        self.assertEqual(
            type(self.get_ad_unit_data(self.tenant, hierarchy_name)),
            AdUnitData)

    def test_create_ad_unit_is_called_when_raw_ad_unit_dict_is_none(self):
        try:
            hierarchy_name = 'g1/educação'
            self.get_ad_unit_data(self.tenant, hierarchy_name)
        except AdUnitNotFoundError:
            self.assertTrue(self.ad_units_client.create_ad_unit.called)

    def test_exception_is_raised_when_status_is_pending(self):
        hierarchy_name = 'g1/educação'

        self.assertRaises(
            AdUnitNotFoundError,
            lambda: self.get_ad_unit_data(self.tenant, hierarchy_name))

    def test_exception_is_raised_when_ad_unit_is_empty_and_status_is_classified(self):
        hierarchy_name = 'g1/agronegócio'

        self.assertRaises(
            AdUnitNotFoundError,
            lambda: self.get_ad_unit_data(self.tenant, hierarchy_name))

    def test_exception_is_raised_when_raw_ad_unit_status_is_ignored(self):
        hierarchy_name = 'g1/globonews'

        self.assertRaises(
            AdUnitIgnoredError,
            lambda: self.get_ad_unit_data(self.tenant, hierarchy_name))
