import unittest
from unittest.mock import MagicMock, patch
from app.services.cma_api.commercial_client import CommercialClient


class TestCommercialClient(unittest.TestCase):
    def setUp(self):
        self.requests = patch(
            'app.services.cma_api.commercial_client.requests')
        self.requests_mock = self.requests.start()

        url = 'http://cma_api.com'
        token = MagicMock()

        self.commercial_client = CommercialClient(url, token)

        self.tenantId = 'globocom'
        self.category = '123456'
        self.data = {}

    def tearDown(self):
        self.requests.stop()

    def test_get_commercial_item_calls_requests_get(self):
        self.commercial_client.get_commercial_item(self.tenantId, 
                                                   self.category)

        self.assertTrue(self.requests_mock.get.called)

    def test_create_commercial_item_calls_requests_post(self):
        self.commercial_client.create_commercial_item(self.tenantId,
                                                      self.data)

        self.assertTrue(self.requests_mock.post.called)

    def test_update_commercial_item_calls_requests_put(self):
        self.commercial_client.update_commercial_item(self.tenantId,
                                                      self.category,
                                                      self.data)

        self.assertTrue(self.requests_mock.put.called)
