import unittest
from unittest.mock import patch
from datetime import datetime, timedelta
from dotenv import load_dotenv
from app.services.cma_api.access_token_generator import AccessTokenGenerator
from app.utils import access_token_dict


class TestAccessTokenGenerator(unittest.TestCase):
    def setUp(self):
        load_dotenv('.env.test', override=True)

        self.service = patch(
            'app.services.cma_api.access_token_generator.OAuth2Service')
        self.service_mock = self.service.start()

        self.token_generator = AccessTokenGenerator(**access_token_dict())
        self.token_generator._access_token = 'token'

        self.service_mock().get_raw_access_token().json.return_value = {
            'expires_in': 899,
            'token_type': 'bearer',
            'access_token': 'token'}

    def tearDown(self):
        self.service.stop()

    def test_json_is_called_when_token_is_invalid(self):
        self.token_generator._expires_at = (datetime.now() -
                                            timedelta(seconds=1))

        self.token_generator.access_token()

        self.assertTrue(
            self.service_mock().get_raw_access_token().json.called)

    def test_json_is_not_called_when_token_is_valid(self):
        self.token_generator._expires_at = (datetime.now() +
                                            timedelta(seconds=899))

        self.token_generator.access_token()

        self.assertFalse(
            self.service_mock().get_raw_access_token().json.called)
