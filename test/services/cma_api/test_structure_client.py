import unittest
from unittest.mock import MagicMock, patch
from app.services.cma_api.structure_client import StructureClient


class TestStructureClient(unittest.TestCase):
    def setUp(self):
        self.requests = patch(
            'app.services.cma_api.structure_client.requests')
        self.requests_mock = self.requests.start()
        self.requests_mock.get().json.return_value = {}

        url = 'http://cma_api.com'
        token = MagicMock()

        self.structure_client = StructureClient(url, token)

    def tearDown(self):
        self.requests.stop()

    def test_get_structure_items_calls_requests_get(self):
        self.structure_client.get_structure_items('g1')

        self.assertTrue(self.requests_mock.get.called)