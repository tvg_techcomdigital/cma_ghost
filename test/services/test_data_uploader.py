from app.services.data_uploader import DataUploader
import unittest
from unittest.mock import MagicMock
from io import BytesIO


class TestDataUploader(unittest.TestCase):
    def setUp(self):
        self.file_uploader_client = MagicMock()
        file = BytesIO
        bucket = 'bucket'
        filename = 'filename'

        self.data_uploader = DataUploader(self.file_uploader_client, file,
                                          bucket, filename)

    def test_mail_client_send_email_is_called(self):
        self.data_uploader.upload_file()

        self.assertTrue(self.file_uploader_client.upload_fileobj.called)
