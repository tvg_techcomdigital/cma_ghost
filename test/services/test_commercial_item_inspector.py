import unittest
import json
from unittest.mock import MagicMock
from os.path import abspath, dirname, join
from app.factories.commercial_item_factory import CommercialItemFactory
from app.models.ad_unit_data import AdUnitData
from app.models.errors import (AdUnitNotFoundError, AdUnitIgnoredError,
                               CommercialItemNotFoundError, Error)
from app.models.commercial_item import CommercialItem
from app.models.structure_item import StructureItem
from app.services.commercial_item_inspector import CommercialItemInspector
from dotenv import load_dotenv


class TestCommercialItemInspector(unittest.TestCase):
    def setUp(self):
        load_dotenv('.env.test', override=True)

        structure_item_data_file = join(
            dirname(abspath(__file__)),
            '../fixtures/services/commercial_item_inspector/structure_item.json')

        with open(structure_item_data_file) as f:
            self.structure_item = StructureItem(**json.load(f))

        self.commercial_client = MagicMock()
        self.ad_unit_data_fetcher = MagicMock()
        self.inspector = CommercialItemInspector(self.structure_item,
                                                 self.commercial_client,
                                                 self.ad_unit_data_fetcher)
        self.ad_unit_data = AdUnitData(
            ad_unit='g1', complemento_ad_unit='', hierarchy_name='g1')

    def test_error_is_returned_when_ad_unit_is_not_found(self):
        error = AdUnitNotFoundError({'hierarchy_name': 'g1'})
        self.ad_unit_data_fetcher.get_ad_unit_data.side_effect = error

        self.assertEqual(self.inspector.get_commercial_item().description,
                         AdUnitNotFoundError.DESCRIPTION)

    def test_error_is_returned_when_ad_unit_is_not_ignored(self):
        error = AdUnitIgnoredError({'hierarchy_name': 'g1'})
        self.ad_unit_data_fetcher.get_ad_unit_data.side_effect = error

        self.assertEqual(self.inspector.get_commercial_item().description,
                         AdUnitIgnoredError.DESCRIPTION)

    def test_create_commercial_item_is_called_when_item_is_not_found(self):
        error = CommercialItemNotFoundError({'category': '123'})
        self.commercial_client.get_commercial_item.side_effect = error, {}

        self.inspector.get_commercial_item()

        self.assertTrue(self.commercial_client.create_commercial_item.called)

    def test_update_commercial_item_is_not_called_when_item_is_correct(self):
        self.ad_unit_data_fetcher.get_ad_unit_data\
            .return_value = self.ad_unit_data

        correct_commercial_item = CommercialItemFactory(
            self.structure_item, self.ad_unit_data).create()

        self.commercial_client.get_commercial_item\
            .return_value = correct_commercial_item.dict

        self.inspector.get_commercial_item()

        self.assertFalse(self.commercial_client.update_commercial_item.called)

    def test_update_commercial_item_is_called_when_item_is_incorrect(self):
        self.ad_unit_data_fetcher.get_ad_unit_data\
            .return_value = self.ad_unit_data

        incorrect_commercial_item = CommercialItem().dict

        self.commercial_client.get_commercial_item\
            .return_value = incorrect_commercial_item

        self.inspector.get_commercial_item()

        self.assertTrue(self.commercial_client.update_commercial_item.called)
