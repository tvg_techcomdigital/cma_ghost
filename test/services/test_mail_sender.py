from app.services.mail_sender import MailSender
import unittest
from unittest.mock import MagicMock


class TestMailSender(unittest.TestCase):
    def setUp(self):
        self.mail_client = MagicMock()
        sender = 'sender@sender.com'
        recipients = 'recipient1@recipient.com, recipient2@recipient.com'
        message = 'Message'
        self.mail_sender = MailSender(self.mail_client, sender,
                                      recipients, message)

    def test_mail_client_send_email_is_called(self):
        self.mail_sender.send_email()

        self.assertTrue(self.mail_client.send_email.called)
