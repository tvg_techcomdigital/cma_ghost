from app.models.ad_unit_data import AdUnitData
import unittest


class TestAdUnitData(unittest.TestCase):
    def setUp(self):
        self.data = {'ad_unit': 'g1',
                     'complemento_ad_unit': '.Home',
                     'hierarchy_name': 'g1'}

        self.ad_unit_data = AdUnitData(**self.data)

    def test_ad_unit_data_type(self):
        self.assertEqual(type(self.ad_unit_data), AdUnitData)

    def test_ad_unit_corresponds_to_value_given(self):
        self.assertEqual(self.ad_unit_data.ad_unit, self.data['ad_unit'])

    def test_complemento_ad_unit_corresponds_to_value_given(self):
        self.assertEqual(self.ad_unit_data.complemento_ad_unit,
                         self.data['complemento_ad_unit'])

    def test_hierarchy_name_corresponds_to_value_given(self):
        self.assertEqual(self.ad_unit_data.hierarchy_name,
                         self.data['hierarchy_name'])
