from app.models.structure_item import StructureItem
from os.path import abspath, dirname, join
from datetime import datetime
import unittest
import json


class TestAdUnitData(unittest.TestCase):
    def setUp(self):
        item_data_file = join(dirname(abspath(__file__)),
                              '../fixtures/models/structure_item/item.json')
        with open(item_data_file) as f:
            self.data = json.load(f)

        self.structure_item = StructureItem(**self.data)

    def test_structure_item_type(self):
        self.assertEqual(type(self.structure_item), StructureItem)

    def test_id_type_is_str(self):
        self.assertEqual(type(self.structure_item.id), str)

    def test_tenantId_type_is_str(self):
        self.assertEqual(type(self.structure_item.tenantId), str)

    def test_hostname_type_is_str(self):
        self.assertEqual(type(self.structure_item.hostname), str)

    def test_name_type_is_str(self):
        self.assertEqual(type(self.structure_item.name), str)

    def test_slug_type_is_str(self):
        self.assertEqual(type(self.structure_item.slug), str)

    def test_hierarchy_name_is_str(self):
        self.assertEqual(type(self.structure_item.hierarchy_name), str)

    def test_hierarchy_name_value(self):
        self.assertEqual(self.structure_item.hierarchy_name,
                         'g1/pop & arte/música/rock in rio/rock in rio 2017')

    def test_path_type_is_str(self):
        self.assertEqual(type(self.structure_item.path), str)

    def test_folder_type_is_str(self):
        self.assertEqual(type(self.structure_item.folder), str)

    def test_ancestors_type_is_list(self):
        self.assertEqual(type(self.structure_item.ancestors), list)

    def test_parent_type_is_str(self):
        self.assertEqual(type(self.structure_item.parent), str)

    def test_parentCategory_is_str(self):
        self.assertEqual(type(self.structure_item.parentCategory), str)

    def test_parents_type_is_list(self):
        self.assertEqual(type(self.structure_item.parents), list)

    def test_versionId_type_is_str(self):
        self.assertEqual(type(self.structure_item.versionId), str)

    def test_disabled_type_is_bool(self):
        self.assertEqual(type(self.structure_item.disabled), bool)

    def test_usingPath_type_is_bool(self):
        self.assertEqual(type(self.structure_item.usingPath), bool)

    def test_createdBy_type_is_bool(self):
        self.assertEqual(type(self.structure_item.createdBy), str)

    def test_created_type_is_datetime(self):
        self.assertEqual(type(self.structure_item.created), datetime)

    def test_modified_type_is_datetime(self):
        self.assertEqual(type(self.structure_item.modified), datetime)

    def test_modified_is_updated_when_attribute_gets_new_value(self):
        modified = self.structure_item.modified
        self.structure_item.id = 'lala'

        self.assertTrue(self.structure_item.modified >= modified)
