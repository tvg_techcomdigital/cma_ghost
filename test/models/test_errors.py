import unittest
from app.models.errors import (Error, ValidationError, AdUnitNotFoundError,
                               AdUnitIgnoredError,
                               CommercialItemNotFoundError)


class TestError(unittest.TestCase):
    def setUp(self):
        self.error = Error(description='description', data={'field': 'invalid'})

    def test_error_type(self):
        self.assertEqual(type(self.error), Error)

    def test_description_is_not_none(self):
        self.assertIsNotNone(self.error.description)

    def test_data_is_not_none(self):
        self.assertIsNotNone(self.error.data)


class TestValidationError(unittest.TestCase):
    def setUp(self):
        self.exception = ValidationError({'field': 'invalid'})

    def test_exception_type(self):
        self.assertEqual(type(self.exception), ValidationError)

    def test_error_type(self):
        self.assertEqual(type(self.exception.error), Error)

    def test_error_description(self):
        self.assertEqual(self.exception.error.description,
                         ValidationError.DESCRIPTION)


class TestAdUnitNotFoundError(unittest.TestCase):
    def setUp(self):
        self.exception = AdUnitNotFoundError({'field': 'invalid'})

    def test_exception_type(self):
        self.assertEqual(type(self.exception), AdUnitNotFoundError)

    def test_error_type(self):
        self.assertEqual(type(self.exception.error), Error)

    def test_error_description(self):
        self.assertEqual(self.exception.error.description,
                         AdUnitNotFoundError.DESCRIPTION)


class TestAdIgnoredError(unittest.TestCase):
    def setUp(self):
        self.exception = AdUnitIgnoredError({'field': 'invalid'})

    def test_exception_type(self):
        self.assertEqual(type(self.exception), AdUnitIgnoredError)

    def test_error_type(self):
        self.assertEqual(type(self.exception.error), Error)

    def test_error_description(self):
        self.assertEqual(self.exception.error.description,
                         AdUnitIgnoredError.DESCRIPTION)


class TestCommercialItemNotFoundError(unittest.TestCase):
    def setUp(self):
        self.exception = CommercialItemNotFoundError({'field': 'invalid'})

    def test_exception_type(self):
        self.assertEqual(type(self.exception), CommercialItemNotFoundError)

    def test_error_type(self):
        self.assertEqual(type(self.exception.error), Error)

    def test_error_description(self):
        self.assertEqual(self.exception.error.description,
                         CommercialItemNotFoundError.DESCRIPTION)
