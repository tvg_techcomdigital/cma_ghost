from app.models.commercial_item import CommercialItem
from random import choice
from os.path import abspath, dirname, join
from datetime import datetime
import unittest
import json


class TestAdUnitData(unittest.TestCase):
    def setUp(self):
        item_data_file = join(dirname(abspath(__file__)),
                              '../fixtures/models/commercial_item/item.json')
        with open(item_data_file) as f:
            self.data = json.load(f)

        self.commercial_item = CommercialItem(**self.data)

    def test_commercial_item_type(self):
        self.assertEqual(type(self.commercial_item), CommercialItem)

    def test_id_type_is_str(self):
        self.assertEqual(type(self.commercial_item.id), str)

    def test_hierarchy_name_is_str(self):
        self.assertEqual(type(self.commercial_item.hierarchy_name), str)

    def test_ad_unit_type_is_str(self):
        self.assertEqual(type(self.commercial_item.ad_unit), str)

    def test_complemento_ad_unit_type_is_str(self):
        self.assertEqual(type(self.commercial_item.complemento_ad_unit), str)

    def test_tenantId_type_is_str(self):
        self.assertEqual(type(self.commercial_item.tenantId), str)

    def test_category_type_is_str(self):
        self.assertEqual(type(self.commercial_item.category), str)

    def test_tag_manager_type_is_str(self):
        self.assertEqual(type(self.commercial_item.tag_manager), str)

    def test_ad_custom_data_type_is_str(self):
        self.assertEqual(type(self.commercial_item.ad_custom_data), str)

    def test_ad_account_id_type_is_str(self):
        self.assertEqual(type(self.commercial_item.ad_account_id), str)

    def test_ad_cms_id_type_is_str(self):
        self.assertEqual(type(self.commercial_item.ad_cms_id), str)

    def test_created_type_is_datetime(self):
        self.assertEqual(type(self.commercial_item.created), datetime)

    def test_modified_type_is_datetime(self):
        self.assertEqual(type(self.commercial_item.modified), datetime)

    def test_modified_is_updated_when_attribute_gets_new_value(self):
        modified = self.commercial_item.modified
        self.commercial_item.ad_unit = 'lala'

        self.assertTrue(self.commercial_item.modified >= modified)

    def test_commercial_item_is_equal_to_itself(self):
        self.assertEqual(self.commercial_item, self.commercial_item)

    def test_commercial_item_differente_to_itself_is_an_empty_dict(self):
        self.assertEqual(self.commercial_item - self.commercial_item, {})

    def test_commercial_dict_has_type_dict(self):
        self.assertEqual(type(self.commercial_item.dict), dict)
