from app.models.structure_item import StructureItem
from app.presenters.commercial_items_json_presenter import CommercialItemsJSONPresenter
from app.presenters.commercial_items_report_presenter import CommercialItemsReportPresenter
from app.services.ad_units.ad_units_client import AdUnitsClient
from app.services.ad_units.ad_unit_data_fetcher import AdUnitDataFetcher
from app.services.cma_api.access_token_generator import AccessTokenGenerator
from app.services.cma_api.structure_client import StructureClient
from app.services.cma_api.commercial_client import CommercialClient
from app.services.commercial_item_inspector import CommercialItemInspector
from app.services.data_uploader import DataUploader
from app.services.mail_sender import MailSender
from app.utils import (access_token_dict, ad_units_client_config,
                       file_upload_client, mail_templates_env, mail_client)
from datetime import datetime, timedelta
from functools import reduce
from io import BytesIO
from os import environ


class TenantsAnalysis(object):
    def __init__(self, tenants):
        self.tenants = tenants
        self._token_generator = AccessTokenGenerator(**access_token_dict())
        # print('token generated')
        self._structure_client = StructureClient(
            environ['CMA_WORKER_CMA_API_STRUCTURE_URL'], self._access_token)
        # print('structure client')
        self._commercial_client = CommercialClient(
            environ['CMA_WORKER_CMA_API_COMMERCIAL_ITEM_URL'],
            self._access_token)
        # print('commercial client')
        self._ad_units_data_fetcher = AdUnitDataFetcher(self._ad_units_client())
        # print('ad units data fetcher')
        self._ref_datetime = datetime.utcnow()

    def run(self):
        commercial_items = [self._get_commercial_item(structure_item)
                            for structure_item in self._structure_items()]
        print('run: commercial_items')
        commercial_items_json = self._commercial_items_json(commercial_items)
        print('run: commercial_items_json')
        commercial_items_report = self._commercial_items_report(commercial_items)
        print('run: commercial_report')
        self._save_json_report(commercial_items_json)
        print('run: save_json_report')
        self._send_report_mail_message(commercial_items_report)
        print('run: send_report_mail_message')

    def _structure_items(self):
        return reduce(lambda acc, cur: acc + cur,
                      [self._tenant_structure_items(t) for t in self.tenants])

    def _tenant_structure_items(self, tenant):
        return list(map(lambda data: StructureItem(**data),
                        self._structure_client.get_structure_items(tenant)))

    def _get_commercial_item(self, structure_item):
        return CommercialItemInspector(
            structure_item, self._commercial_client,
            self._ad_units_data_fetcher).get_commercial_item()

    def _access_token(self):
        return self._token_generator.access_token()

    def _ad_units_client(self):
        return AdUnitsClient(**ad_units_client_config())

    def _commercial_items_json(self, commercial_items):
        return CommercialItemsJSONPresenter(commercial_items).generate_json()

    def _commercial_items_report(self, commercial_items):
        return CommercialItemsReportPresenter(
            commercial_items, self._ref_datetime).generate_report()

    def _send_report_mail_message(self, commercial_items_report):
        template = mail_templates_env().get_template('result_template.html')

        message = template.render(
            current_datetime=datetime.now(),
            commercial_item_edit_url=environ[
                'CMA_WORKER_CMA_COMMERCIAL_ITEM_EDIT_URL'],
            ad_units_url=environ['CMA_WORKER_GS_SERVICE_AD_UNITS_URL'],
            report=commercial_items_report)

        MailSender(
            mail_client(), environ['CMA_WORKER_REPORT_SENDER'],
            environ['CMA_WORKER_REPORT_RECIPIENTS'], message).send_email()

    def _save_json_report(self, commercial_items_json):
        report_file = BytesIO(commercial_items_json.encode('utf8'))
        report_filename = 'cma_worker_report_%s.json' % (
            datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ'))

        DataUploader(file_upload_client(), report_file,
                     environ['CMA_WORKER_AWS_BUCKET_NAME'],
                     report_filename).upload_file()
