from app.models.commercial_item import CommercialItem
from os import environ
from app.utils import special_caract_ad_custom_data

class CommercialItemFactory(object):
    def __init__(self, structure_item, ad_unit_data):
        self.structure_item = structure_item
        self.ad_unit_data = ad_unit_data

    def create(self):
        return CommercialItem(**self._commercial_item_dict())

    def _commercial_item_dict(self):
        return {
            'hierarchy_name': self.ad_unit_data.hierarchy_name,
            'ad_unit': self.ad_unit_data.ad_unit,
            'complemento_ad_unit': self.ad_unit_data.complemento_ad_unit,
            'tenantId': self.structure_item.tenantId,
            'category': self.structure_item.id,
            'tag_manager': self._tag_manager(),
            'ad_custom_data': self._ad_custom_data(),
            'ad_account_id': environ['CMA_WORKER_CMA_API_AD_ACCOUNT_ID'],
            'ad_cms_id': environ['CMA_WORKER_CMA_API_AD_CMS_ID']}

    def _tag_manager(self):
        return '%s%s/prod/utag.js' % (
            environ['CMA_WORKER_CMA_API_TAG_MANAGER_URL_BASE'],
            self.structure_item.tenantId)

    def _ad_custom_data(self):
        return 'tvg_pgStr=%s' % self.ad_unit_data.hierarchy_name