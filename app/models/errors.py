from collections import namedtuple

Error = namedtuple('Error', ['description', 'data'])


class ValidationError(Exception):
    DESCRIPTION = 'validation error'

    def __init__(self, data):
        self.error = Error(description=self.__class__.DESCRIPTION, data=data)


class AdUnitNotFoundError(ValidationError):
    DESCRIPTION = 'ad unit not found'


class AdUnitIgnoredError(ValidationError):
    DESCRIPTION = 'ad unit ignored'


class CommercialItemNotFoundError(ValidationError):
    DESCRIPTION = 'commercial item not found'
