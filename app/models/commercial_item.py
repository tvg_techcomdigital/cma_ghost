from datetime import datetime


class CommercialItem(object):
    def __init__(self, id=None, hierarchy_name=None, ad_unit=None,
                 complemento_ad_unit=None, tenantId=None, category=None,
                 tag_manager=None, ad_custom_data=None, ad_account_id=None,
                 ad_cms_id=None, created=None, modified=None, **kwargs):
        self.id = id
        self.hierarchy_name = hierarchy_name
        self.ad_unit = ad_unit
        self.complemento_ad_unit = complemento_ad_unit
        self.tenantId = tenantId
        self.category = category
        self.tag_manager = tag_manager
        self.ad_custom_data = ad_custom_data
        self.ad_account_id = ad_account_id
        self.ad_cms_id = ad_cms_id
        self.created = self._datetime_obj(created)
        self.modified = self._datetime_obj(modified)

    def __setattr__(self, attr, value):
        if (attr != 'modified'):
            self.modified = datetime.utcnow()
        super().__setattr__(attr, value)

    def __eq__(self, other):
        return self.dict == other.dict

    def __sub__(self, other):
        return dict(self.dict.items() - other.dict.items())

    def __repr__(self):
        return 'CommercialItem(id=%r, hierarchy_name=%r, ad_unit=%r, '\
            'complemento_ad_unit=%r, tenantId=%r, category=%r, '\
            'tag_manager=%r, ad_custom_data=%r, ad_account_id=%r, '\
            'ad_cms_id=%r, created=%r, modified=%r)' % (
                self.id, self.hierarchy_name, self.ad_unit,
                self.complemento_ad_unit, self.tenantId, self.category,
                self.tag_manager, self.ad_custom_data, self.ad_account_id,
                self.ad_cms_id, self.created, self.modified)

    @property
    def dict(self):
        blacklisted_fields = ['id', 'created', 'modified']
        return {k: v for k, v in self.__dict__.items()
                if k not in blacklisted_fields}

    def _datetime_obj(self, value):
        if value is None:
            return datetime.utcnow()

        return (value if type(value) == datetime
                else datetime.strptime(value, '%Y-%m-%dT%H:%M:%S.%fZ'))
