from datetime import datetime


class StructureItem(object):
    def __init__(self, id=None, tenantId=None, hostname=None, name=None,
                 slug=None, hierarchy_name=None, path=None, folder=None,
                 ancestors=[], parent=None, parentCategory=None, parents=[],
                 versionId=None, disabled=None, usingPath=None,
                 createdBy=None, created=None, modified=None, **kwargs):
        self.id = id
        self.tenantId = tenantId
        self.hostname = hostname
        self.name = name
        self.slug = slug
        self.hierarchy_name = self._parsed_hierarchy_name(hierarchy_name)
        self.path = path
        self.folder = folder
        self.ancestors = ancestors
        self.parent = parent
        self.parentCategory = parentCategory
        self.parents = parents
        self.versionId = versionId
        self.disabled = disabled
        self.usingPath = usingPath
        self.createdBy = createdBy
        self.created = self._datetime_obj(created)
        self.modified = self._datetime_obj(modified)

    def __setattr__(self, attr, value):
        if (attr != 'modified'):
            self.modified = datetime.utcnow()
        super().__setattr__(attr, value)

    def __repr__(self):
        return 'StructureItem(id=%r, tenantId=%r, hostname=%r, name=%r, '\
            'slug=%r, hierarchy_name=%r, path=%r, folder=%r, ancestors=%r, '\
            'parent=%r, parentCategory=%r, parents=%r, versionId=%r, '\
            'disabled=%r, usingPath=%r, createdBy=%r, created=%r, '\
            'modified=%r)' % (
                self.id, self.tenantId, self.hostname, self.name, self.slug,
                self.hierarchy_name, self.path, self.folder, self.ancestors,
                self.parent, self.parentCategory, self.parents,
                self.versionId, self.disabled, self.usingPath, self.createdBy,
                self.created, self.modified)

    def _parsed_hierarchy_name(self, hierarchy_name):
        if hierarchy_name is None:
            return

        return '/'.join([part.lower().strip()
                         for part in hierarchy_name.split('/')])

    def _datetime_obj(self, value):
        if value is None:
            return datetime.utcnow()

        return (value if type(value) == datetime
                else datetime.strptime(value, '%Y-%m-%dT%H:%M:%S.%fZ'))
