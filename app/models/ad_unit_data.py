from collections import namedtuple

_fields = ('ad_unit', 'complemento_ad_unit', 'hierarchy_name')

AdUnitData = namedtuple('AdUnitData', _fields)
