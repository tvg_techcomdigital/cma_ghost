from app.models.ad_unit_data import AdUnitData
from app.models.errors import AdUnitNotFoundError, AdUnitIgnoredError


class AdUnitDataFetcher(object):
    PENDING = 'PENDENTE'
    IGNORED = 'IGNORADO'

    def __init__(self, ad_units_client):
        self._ad_units_client = ad_units_client
        self._ad_units = {}

    def get_ad_unit_data(self, tenant, hierarchy_name):
        raw_ad_unit = self._raw_ad_unit(tenant, hierarchy_name)

        complemento_ad_unit = '.Home' if '.Home' in raw_ad_unit else ''
        ad_unit = raw_ad_unit.replace(complemento_ad_unit, '')

        return AdUnitData(ad_unit=ad_unit,
                          complemento_ad_unit=complemento_ad_unit,
                          hierarchy_name=hierarchy_name)

    def _raw_ad_unit(self, tenant, hierarchy_name):
        raw_ad_unit_dict = self._tenant_raw_ad_units(tenant)\
            .get(hierarchy_name)

        if raw_ad_unit_dict is None:
            self._create_new_raw_ad_unit(tenant, hierarchy_name)
            raise AdUnitNotFoundError({'hierarchy_name': hierarchy_name,
                                       'tenant': tenant})
        elif raw_ad_unit_dict['status'] == self.__class__.IGNORED:
            raise AdUnitIgnoredError({'hierarchy_name': hierarchy_name,
                                      'tenant': tenant})
        elif (not raw_ad_unit_dict.get('ad_unit')
              or raw_ad_unit_dict['status'] == self.__class__.PENDING):
            raise AdUnitNotFoundError({'hierarchy_name': hierarchy_name,
                                       'tenant': tenant})

        return raw_ad_unit_dict['ad_unit']

    def _create_new_raw_ad_unit(self, tenant, hierarchy_name):
        self._ad_units_client.create_ad_unit(
            tenant, {'hierarchy_name': hierarchy_name,
                     'status': self.__class__.PENDING})
        self._ad_units[tenant][hierarchy_name] = {
            'ad_unit': '', 'status': self.__class__.PENDING}

    def _tenant_raw_ad_units(self, tenant):
        if tenant not in self._ad_units:
            self._ad_units[tenant] = self._ad_units_client\
                .get_ad_units(tenant)
        return self._ad_units[tenant]
