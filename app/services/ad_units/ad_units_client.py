import httplib2
from googleapiclient import discovery
from oauth2client.service_account import ServiceAccountCredentials


class AdUnitsClient(object):
    DATA_RANGE = 'A2:C'
    DATA_KEYS = ('hierarchy_name', 'ad_unit', 'status')

    def __init__(self, service_name, service_version, discovery_url,
                 credentials_dict, scopes, spreadsheet_id):
        self.service_name = service_name
        self.service_version = service_version
        self.discovery_url = discovery_url
        self.credentials = self._credentials(credentials_dict, scopes)
        self.spreadsheet_id = spreadsheet_id

    def get_ad_units(self, tenant):
        args = {'spreadsheetId': self.spreadsheet_id,
                'range': self._range_name(tenant)}

        data = self._service().spreadsheets().values().get(**args).execute()
        items = map(lambda row: row + [''] * (3 - len(row)), data['values'])

        return {hierarchy_name: {'ad_unit': ad_unit, 'status': status}
                for hierarchy_name, ad_unit, status in items
                if hierarchy_name}

    def create_ad_unit(self, tenant, data):
        values = [[data.get(k, '') for k in self.__class__.DATA_KEYS]]

        args = {'spreadsheetId': self.spreadsheet_id,
                'range': self._range_name(tenant),
                'valueInputOption': 'RAW',
                'body': {'values': values}}

        self._service().spreadsheets().values().append(**args).execute()

    def _credentials(self, credentials_dict, scopes):
        return ServiceAccountCredentials.from_json_keyfile_dict(
            credentials_dict, scopes)

    def _service(self):
        return discovery.build(
            self.service_name, self.service_version,
            http=self.credentials.authorize(httplib2.Http()),
            discoveryServiceUrl=self.discovery_url)

    def _range_name(self, tenant):
        return '%s!%s' % (tenant, self.__class__.DATA_RANGE)
