class DataUploader(object):
    def __init__(self, file_upload_client, file, bucket, filename,
                 content_type=None):
        self.file_upload_client = file_upload_client
        self.file = file
        self.bucket = bucket
        self.filename = filename
        self.content_type = (content_type if content_type
                             else 'application/json')

    def upload_file(self):
        self.file_upload_client.upload_fileobj(
            self.file, self.bucket, self.filename,
            ExtraArgs=self._extra_args())

    def _extra_args(self):
        return {'ACL': 'authenticated-read', 'ContentType': self.content_type}
