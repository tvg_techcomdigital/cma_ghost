from app.models.commercial_item import CommercialItem
from app.models.errors import (AdUnitNotFoundError, AdUnitIgnoredError,
                               CommercialItemNotFoundError)
from app.factories.commercial_item_factory import CommercialItemFactory
from time import localtime, strftime


class CommercialItemInspector(object):
    def __init__(self, structure_item, commercial_client,
                 ad_unit_data_fetcher):
        self.structure_item = structure_item
        self.commercial_client = commercial_client
        self.ad_unit_data_fetcher = ad_unit_data_fetcher

    def get_commercial_item(self):
        try:
            expected_commercial_item = self._expected_commercial_item()
            commercial_item = self._commercial_item()
            if commercial_item == expected_commercial_item:
                return commercial_item
            repaired_data = expected_commercial_item - commercial_item
            print('--------------------Given-----------------------')
            print(commercial_item)
            print('--------------------Expected--------------------')
            print(expected_commercial_item)
            print('--------------------Repaired--------------------')
            print(repaired_data)
            print('----------------------------------------')
            self.commercial_client.update_commercial_item(
                self.structure_item.tenantId,
                commercial_item.id, 
                repaired_data)
            return self._commercial_item()
        except (AdUnitNotFoundError, AdUnitIgnoredError) as e:
            return e.error
        except CommercialItemNotFoundError as e:
            self.commercial_client.create_commercial_item(
                self.structure_item.tenantId,
                expected_commercial_item.dict)
            return self._commercial_item()

    def _ad_unit_data(self):
        return self.ad_unit_data_fetcher.get_ad_unit_data(
            self.structure_item.tenantId, self.structure_item.hierarchy_name)

    def _commercial_item(self):
        commercial_item_api_data = self.commercial_client\
            .get_commercial_item(self.structure_item.tenantId,
                                 self.structure_item.id)

        return CommercialItem(**{
            **commercial_item_api_data,
            **{'hierarchy_name': self.structure_item.hierarchy_name}})

    def _expected_commercial_item(self):
        return CommercialItemFactory(self.structure_item,
                                     self._ad_unit_data()).create()
