class MailSender(object):
    def __init__(self, mail_client, sender, recipients, message):
        self._mail_client = mail_client
        self._sender = sender
        self._recipients = recipients
        self._message = message

    def send_email(self):
        return self._mail_client.send_email(
            Source=self._sender,
            Destination=self._destination(),
            Message=self._email_message())

    def _destination(self):
        return {'ToAddresses': self._recipients.split(',')}

    def _email_message(self):
        return {'Subject': {'Data': 'Ghost', 'Charset': 'UTF-8'},
                'Body': {
                    'Text': {'Data': self._message, 'Charset': 'UTF-8'},
                    'Html': {'Data': self._message, 'Charset': 'UTF-8'}}}
