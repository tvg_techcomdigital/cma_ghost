import requests
from time import localtime, strftime

class StructureClient(object):
    PAGES_LIMIT = 1000

    def __init__(self, url, token):
        self.url = url
        self.token = token

    def get_structure_items(self, tenant):
        structure_items = []
        page = 1

        while True:
            new_items = self._page_items(tenant, page)

            if not new_items:
                break

            structure_items.extend(new_items)
            page += 1
            print(strftime("%H:%M:%S", localtime()), '\ttenant & page: ', tenant, page)

        return structure_items

    def _page_items(self, tenant, page):
        response = requests.get(
            '%s/%s' % (self.url, self._query_string(tenant, page)),
            headers=self._headers())

        if response.json().get('items') == None:
            print('structure_client with no items in response: ', response.json())
        
        return response.json().get('items', [])

    def _query_string(self, tenant, page):
        return '%s/?filter[page]=%s&filter[limit]=%s' % (
                tenant, page, self.__class__.PAGES_LIMIT)

    def _headers(self):
        return {
            'Authorization': 'Bearer %s' % self.token(),
            'Accept': 'application/json'}
