from rauth import OAuth2Service
from datetime import datetime, timedelta


class AccessTokenGenerator(object):
    def __init__(self, client_id, client_secret, access_token_url,
                 authorize_url, base_url, redirect_uri):
        self.service = OAuth2Service(
            name='foo',
            client_id=client_id,
            client_secret=client_secret,
            access_token_url=access_token_url,
            authorize_url=authorize_url,
            base_url=base_url)
        self.redirect_uri = redirect_uri
        self._access_token = None
        self._expires_at = None

    def access_token(self):
        if self._access_token_is_valid():
            return self._access_token

        cur_datetime = datetime.now()
        raw_access_token = self.service.get_raw_access_token(
            data=self._auth_data()).json()

        self._expires_at = cur_datetime + \
            timedelta(seconds=int(raw_access_token['expires_in']))
        self._access_token = raw_access_token['access_token']
        return self._access_token

    def _access_token_is_valid(self):
        if self._expires_at is None or self._access_token is None:
            return False

        return datetime.now() + timedelta(minutes=1) <= self._expires_at

    def _auth_data(self):
        return {'code': 'bar',
                'grant_type': 'client_credentials',
                'redirect_uri': self.redirect_uri}
