import requests
import json
from app.models.errors import CommercialItemNotFoundError


class CommercialClient(object):
    def __init__(self, url, token):
        self.url = url
        self.token = token

    def get_commercial_item(self, tenantId, category):
        try:
            response = requests.get(
                '%s/%s?filter[where][category]=%s' %
                (self.url, tenantId, category),
                headers=self._headers())

            if response.json().get('items') == None:
                print('commercial_item with no items in response: ', response.json())

            return response.json()['items'][0]
        except (KeyError, IndexError) as e:
            if response.status_code not in (200, 404):
                raise e

            raise CommercialItemNotFoundError({'category': category})

    def create_commercial_item(self, tenantId, data):
        response = requests.post('%s/%s' % (self.url, tenantId),
                                 headers=self._headers(),
                                 data=json.dumps(data))

        return response.ok

    def update_commercial_item(self, tenantId, category, data):
        response = requests.put(
            '%s/%s/%s/' % (self.url, tenantId, category),
            headers=self._headers(), data=json.dumps(data))

        return response.ok

    def _headers(self):
        return {'Authorization': 'Bearer %s' % self.token(),
                'Accept': 'application/json',
                'Content-Type': 'application/json'}
