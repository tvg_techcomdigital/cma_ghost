import requests
import json
from boto3 import session, client

webhookurl = 'https://hooks.slack.com/services/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'

def lambda_handler(event, context):
    # Establish credentials
    session_var = session.Session()
    credentials = session_var.get_credentials()
    region = session_var.region_name or 'us-east-1' 

    if 'detail-type' not in event:
        raise ValueError('ERROR: event object is not a valid CloudWatch Logs event')
    else:
        if event['detail-type'] == 'ECS Task State Change':
            detail = event['detail']
            Message = json.dumps(detail)
            messagedata = json.loads(Message)
            payload = {
                'icon_emoji': 'ghost:', 
                'username': 'ECSLambda-Post-Bot', 
                'channel': '@aline',
                'text': ''
            }
            message_fields = {
                'container_name': messagedata['containers'][0]['name'],
                'containerLastStatus': messagedata['containers'][0]['lastStatus'],
                'createdAt': messagedata['createdAt'],
                'desiredStatus': messagedata['desiredStatus'],
                'lastStatus': messagedata['lastStatus'],
                'updatedAt': messagedata['updatedAt']
            }
            
            if detail['lastStatus'] == 'STOPPED':
                # if detail['stoppedReason'] == 'Essential container in task exited':
                message_fields.update(
                    {'stoppedReason': messagedata['stoppedReason']})
                    
                # ALL DATA that was posted from cloudwatch ## 
                # payload['text'] = str(messagedata)
            
            payload['text'] = json.dumps(message_fields, indent=4)
                
            # POST webhook to Slack
            headers = {'content-type': 'application/json'}
            r = requests.put(
                webhookurl, 
                data=json.dumps(payload), 
                headers=headers
            )
        
            # print(json.dumps(payload))
            print(r.status_code)
            print('Post of data to slack was attempted')


# if __name__ == '__main__':
    # event_ok = {
    #     'version': '0',
    #     'id': '9bcdac79-b31f-4d3d-9410-fbd727c29fab',
    #     'detail-type': 'ECS Task State Change',
    #     'source': 'aws.ecs',
    #     'account': '111122223333',
    #     'time': '2016-12-06T16:41:06Z',
    #     'region': 'us-east-1',
    #     'resources': [
    #         'arn:aws:ecs:us-east-1:111122223333:task/b99d40b3-5176-4f71-9a52-9dbd6f1cebef'
    #     ],
    #     'detail': {
    #         'clusterArn': 'arn:aws:ecs:us-east-1:111122223333:cluster/default',
    #         'containerInstanceArn': 'arn:aws:ecs:us-east-1:111122223333:container-instance/b54a2a04-046f-4331-9d74-3f6d7f6ca315',
    #         'containers': [
    #         {
    #             'containerArn': 'arn:aws:ecs:us-east-1:111122223333:container/3305bea1-bd16-4217-803d-3e0482170a17',
    #             'exitCode': 0,
    #             'lastStatus': 'STOPPED',
    #             'name': 'xray',
    #             'taskArn': 'arn:aws:ecs:us-east-1:111122223333:task/b99d40b3-5176-4f71-9a52-9dbd6f1cebef'
    #         }
    #         ],
    #         'createdAt': '2016-12-06T16:41:05.702Z',
    #         'desiredStatus': 'RUNNING',
    #         'group': 'task-group',
    #         'lastStatus': 'STOP', # 'RUNNING',
    #         'overrides': {
    #         'containerOverrides': [
    #             {
    #             'name': 'xray'
    #             }
    #         ]
    #         },
    #         'startedAt': '2016-12-06T16:41:06.8Z',
    #         'startedBy': 'ecs-svc/9223370556150183303',
    #         'updatedAt': '2016-12-06T16:41:06.975Z',
    #         'taskArn': 'arn:aws:ecs:us-east-1:111122223333:task/b99d40b3-5176-4f71-9a52-9dbd6f1cebef',
    #         'taskDefinitionArn': 'arn:aws:ecs:us-east-1:111122223333:task-definition/xray:2',
    #         'version': 4
    #     }
    # }

    # event_error = {
    #     'version': '0',
    #     'id': '8f07966c-b005-4a0f-9ee9-63d2c41448b3',
    #     'detail-type': 'ECS Task State Change',
    #     'source': 'aws.ecs',
    #     'account': '244698725403',
    #     'time': '2016-10-17T20:29:14Z',
    #     'region': 'us-east-1',
    #     'resources': [
    #         'arn:aws:ecs:us-east-1:123456789012:task/cdf83842-a918-482b-908b-857e667ce328'
    #     ],
    #     'detail': {
    #         'clusterArn': 'arn:aws:ecs:us-east-1:123456789012:cluster/eventStreamTestCluster',
    #         'containerInstanceArn': 'arn:aws:ecs:us-east-1:123456789012:container-instance/f813de39-e42c-4a27-be3c-f32ebb79a5dd',
    #         'containers': [
    #         {
    #             'containerArn': 'arn:aws:ecs:us-east-1:123456789012:container/4b5f2b75-7d74-4625-8dc8-f14230a6ae7e',
    #             'exitCode': 1,
    #             'lastStatus': 'STOPPED',
    #             'name': 'web',
    #             'networkBindings': [
    #             {
    #                 'bindIP': '0.0.0.0',
    #                 'containerPort': 80,
    #                 'hostPort': 80,
    #                 'protocol': 'tcp'
    #             }
    #             ],
    #             'taskArn': 'arn:aws:ecs:us-east-1:123456789012:task/cdf83842-a918-482b-908b-857e667ce328'
    #         }
    #         ],
    #         'createdAt': '2016-10-17T20:28:53.671Z',
    #         'desiredStatus': 'STOPPED',
    #         'lastStatus': 'STOPPED',
    #         'overrides': {
    #         'containerOverrides': [
    #             {
    #             'name': 'web'
    #             }
    #         ]
    #         },
    #         'startedAt': '2016-10-17T20:29:14.179Z',
    #         'stoppedAt': '2016-10-17T20:29:14.332Z',
    #         'stoppedReason': 'Essential container in task exited',
    #         'updatedAt': '2016-10-17T20:29:14.332Z',
    #         'taskArn': 'arn:aws:ecs:us-east-1:123456789012:task/cdf83842-a918-482b-908b-857e667ce328',
    #         'taskDefinitionArn': 'arn:aws:ecs:us-east-1:123456789012:task-definition/wpunconfiguredfail:1',
    #         'version': 3
    #     }
    # }

    # # lambda_handler(event_ok, 'x')
    # lambda_handler(event_error, 'x')