import json
from datetime import datetime
from app.models.errors import Error
from app.models.commercial_item import CommercialItem


class CommercialItemsJSONPresenter(object):
    def __init__(self, commercial_items):
        self.commercial_items = commercial_items

    def generate_json(self):
        items = [self._json_dict(item) for item in self.commercial_items]
        return json.dumps(items, ensure_ascii=False)

    def _json_dict(self, obj):
        if not isinstance(obj, (CommercialItem, Error)):
            raise TypeError

        return (self._error_dict(obj) if type(obj) == Error
                else self._commercial_item_dict(obj))

    def _error_dict(self, obj):
        return {Error.__name__: obj._asdict()}

    def _commercial_item_dict(self, obj):
        item_dict =  {k: v.isoformat() if isinstance(v, datetime) else v
                      for k, v in obj.__dict__.items()}
        return {CommercialItem.__name__: item_dict}
