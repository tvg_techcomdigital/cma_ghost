from app.models.errors import Error, AdUnitNotFoundError
from app.models.commercial_item import CommercialItem
from functools import reduce


class CommercialItemsReportPresenter(object):
    AD_UNITS_NOT_FOUND = 'ad units not found'
    COMMERCIAL_ITEMS_CREATED = 'commercial items created'
    COMMERCIAL_ITEMS_UPDATED = 'commercial items updated'

    def __init__(self, commercial_items, ref_datetime):
        self.commercial_items = commercial_items
        self.ref_datetime = ref_datetime

    def generate_report(self):
        results = {self.__class__.AD_UNITS_NOT_FOUND: [],
                   self.__class__.COMMERCIAL_ITEMS_CREATED: [],
                   self.__class__.COMMERCIAL_ITEMS_UPDATED: []}

        return reduce(self._append_to_key, self.commercial_items, results)

    def _append_to_key(self, acc, cur):
        key = self._item_classification(cur)
        return {**acc, **{key: acc[key] + [cur]}} if key else acc

    def _item_classification(self, item):
        return {Error: self._error_item_key,
                CommercialItem: self._commercial_item_key}[type(item)](item)

    def _error_item_key(self, item):
        if item.description == AdUnitNotFoundError.DESCRIPTION:
            return self.__class__.AD_UNITS_NOT_FOUND

    def _commercial_item_key(self, item):
        if item.created >= self.ref_datetime:
            return self.__class__.COMMERCIAL_ITEMS_CREATED

        if item.modified >= self.ref_datetime:
            return self.__class__.COMMERCIAL_ITEMS_UPDATED
