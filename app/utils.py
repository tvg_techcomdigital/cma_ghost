import json
import boto3
from os import environ
from os.path import abspath, dirname, join
from jinja2 import Environment, PackageLoader, select_autoescape


def aws_credentials():
    return {'aws_access_key_id': environ['CMA_WORKER_AWS_ACCESS_KEY'],
            'aws_secret_access_key': environ['CMA_WORKER_AWS_SECRET_KEY']}


def mail_client():
    return boto3.client('ses', environ['CMA_WORKER_AWS_REGION'],
                        **aws_credentials())


def file_upload_client():
    return boto3.client('s3', environ['CMA_WORKER_AWS_REGION'],
                        **aws_credentials())


def mail_templates_env():
    return Environment(
        loader=PackageLoader('app', 'templates'),
        autoescape=select_autoescape(enabled_extensions=('html', 'htm')))


def access_token_dict():
    return {
        'client_id': environ['CMA_WORKER_CMA_API_CLIENT_ID'],
        'client_secret': environ['CMA_WORKER_CMA_API_CLIENT_SECRET'],
        'access_token_url': environ['CMA_WORKER_CMA_API_ACCESS_TOKEN_URL'],
        'authorize_url': environ['CMA_WORKER_CMA_API_AUTHORIZE_URL'],
        'base_url': environ['CMA_WORKER_CMA_API_BASE_URL'],
        'redirect_uri': environ['CMA_WORKER_CMA_API_REDIRECT_URI']}


def ad_units_client_config():
    with open(join(dirname(abspath(__file__)), '../credentials.json')) as f:
        credentials_dict = json.load(f)

    return {'service_name': environ['CMA_WORKER_GS_SERVICE_NAME'],
            'service_version': environ['CMA_WORKER_GS_SERVICE_VERSION'],
            'discovery_url': environ['CMA_WORKER_GS_DISCOVERY_URL'],
            'credentials_dict': credentials_dict,
            'scopes': environ['CMA_WORKER_GS_SCOPES'],
            'spreadsheet_id': environ['CMA_WORKER_GS_SPREADSHEET_ID']}

def special_caract_ad_custom_data():
    return ['"', '\'', '=', '!', '+', '#', '*', '~', 
            ';', '^', '(', ')', '<', '>', '[', ']']
            
