import argparse
from app.tenants_analysis import TenantsAnalysis
from os import environ
from dotenv import load_dotenv

if environ.get('CMA_WORKER_ENV') != 'production':
    load_dotenv('.env', override=True)
    # load_dotenv('.env.test', override=True)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Retrieve and store data from Backstage CMA REST API.')

    parser.add_argument('--tenants', nargs='+', help='g1 ge gshow, etc.')

    parsed_args = parser.parse_args()

    TenantsAnalysis(parsed_args.tenants).run()