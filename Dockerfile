FROM centos/python-36-centos7

WORKDIR /usr/src

COPY requirements.txt requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt

COPY app/ app/
COPY bin/ bin/
COPY test/ test/
COPY .env .env
COPY credentials.json credentials.json
COPY run.py run.py

EXPOSE 80
 
CMD ["python", "run.py", "--tenants", "g1", "ge", "gshow"]