from app.models.errors import Error
from app.models.commercial_item import CommercialItem
from app.services.cma_api.access_token_generator import AccessTokenGenerator
from app.services.cma_api.structure_client import StructureClient
from app.services.cma_api.commercial_client import CommercialClient
from app.services.structure_analyzer import StructureAnalyzer
from app.services.ad_units.ad_units_client import AdUnitsClient
from app.services.ad_units.ad_unit_data_fetcher import AdUnitDataFetcher
from app.utils import access_token_dict, ad_units_client_config
from datetime import datetime, timedelta
from os import environ
from dotenv import load_dotenv

load_dotenv('.env', override=True)

token_generator = AccessTokenGenerator(**access_token_dict())

ad_units_client = AdUnitsClient(**ad_units_client_config())

ad_units_data_fetcher = AdUnitDataFetcher(ad_units_client)

structure_client = StructureClient(environ['CMA_STRUCTURE_API_URL'],
                                   token_generator.access_token)

commercial_client = CommercialClient(environ['CMA_COMERCIAL_API_URL'],
                                     token_generator.access_token)
tenant = 'gshow'


structure_analyzer = StructureAnalyzer(tenant, structure_client,
                                       commercial_client, ad_units_data_fetcher)

ref_datetime = datetime.utcnow() - timedelta(hours=1)
xx = structure_analyzer.commercial_items()
