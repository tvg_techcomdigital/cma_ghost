import json
import hashlib
import logging
import datetime
from os import environ
from io import BytesIO
import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
from app.services.mail_sender import MailSender
from app.utils import mail_client
from dotenv import load_dotenv, find_dotenv

# if environ.get('CMA_WORKER_ENV') != 'production':
#     if environ.get('CMA_WORKER_ENV') == 'test':
#         load_dotenv('.env.test', override=True)
#     else:
#         load_dotenv('.env', override=True)
load_dotenv('.env.test', override=True)

processed_values = {}
processed_values['gshow'] = {
    "totals": {
        "missing_structure_trees": 0,
        "matched_structure_tree": 436,
        "comercial_items_to_be_created": 0,
        "matched_ad_units": 436,
        "created_comercial_items": 0,
        "changed_ad_units": 0,
        "unmatched_structure_tree": 342,
        "unmatched_ad_units": 0
    },
    "unmatched_ad_units": [],
    "unmatched_structure_tree": [
        {
            "slug": "megasena",
            "modified": "2017-11-24T21:27:45.716Z",
            "parents": [
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/3fced62b-25c5-41bb-9b24-c8c357e5e5a3",
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/54a4cf1f-64ce-4d7f-b643-9697139a8249"
            ],
            "parent": "54a4cf1f-64ce-4d7f-b643-9697139a8249",
            "structure_tree": "G1 / Loterias / resultados",
            "created": "2017-11-23T13:30:07.234Z",
            "id": "203e3dd2-55be-4c3d-a803-1fc486855fd0",
            "ad_custom_data": "",
            "ad_unit": "",
            "tenantId": "g1",
            "versionId": "dd54f167-4d6e-4194-b4bd-4664cc32e662",
            "path": "loterias/megasena",
            "comercial_id": "f95558a4-24d2-4ebb-83a1-9a63054bb81b",
            "parentCategory": "54a4cf1f-64ce-4d7f-b643-9697139a8249",
            "createdBy": "diego.pinheiro@corp.globo.com",
            "hostname": "g1.globo.com",
            "usingPath": False,
            "disabled": False,
            "hierarchy_name": "G1 / Loterias / resultados",
            "name": "resultados",
            "ancestors": [
                "3fced62b-25c5-41bb-9b24-c8c357e5e5a3",
                "54a4cf1f-64ce-4d7f-b643-9697139a8249"
            ]
        },
        {
            "slug": "especial-publicitario",
            "modified": "2017-11-16T18:24:02.631Z",
            "parent": "1e801343-a5d1-4d1b-b214-0a40852dbd47",
            "ad_custom_data": "tvg_pgStr=g1/economia/educação financeira/especial publicitário",
            "tenantId": "g1",
            "structure_tree": "G1 / Economia / Educação Financeira / Especial Publicitário",
            "parentCategory": "1e801343-a5d1-4d1b-b214-0a40852dbd47",
            "createdBy": "herman@corp.globo.com",
            "hostname": "g1.globo.com",
            "ad_unit": "tvg_G1/Economia/Seu_Dinheiro/Especial_Exclusivo",
            "disabled": False,
            "parents": [
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/3fced62b-25c5-41bb-9b24-c8c357e5e5a3",
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/cf9c1f0c-f737-4dbb-843c-4b2af64be9e1",
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/1e801343-a5d1-4d1b-b214-0a40852dbd47"
            ],
            "name": "Especial Publicitário",
            "ancestors": [
                "3fced62b-25c5-41bb-9b24-c8c357e5e5a3",
                "cf9c1f0c-f737-4dbb-843c-4b2af64be9e1",
                "1e801343-a5d1-4d1b-b214-0a40852dbd47"
            ],
            "path": "economia/educacao-financeira/especial-publicitario",
            "usingPath": True,
            "folder": "https://g1-core.backstage.globoi.com/api/editorias/4619",
            "created": "2017-11-01T13:31:32.978Z",
            "id": "97251e49-4bd5-4eb7-b7c0-2190a08d8a26",
            "versionId": "28e9ce49-1876-483a-9e73-de032c747a6d",
            "hierarchy_name": "G1 / Economia / Educação Financeira / Especial Publicitário",
            "comercial_id": "642d351e-5e0c-4e8f-a90c-33f2cb136c2b"
        },
        {
            "slug": "orama",
            "modified": "2017-11-16T18:22:07.950Z",
            "parent": "97251e49-4bd5-4eb7-b7c0-2190a08d8a26",
            "ad_custom_data": "tvg_pgStr=g1/economia/educação financeira/especial publicitário/especial publicitário - órama",
            "tenantId": "g1",
            "structure_tree": "G1 / Economia / Educação Financeira / Especial Publicitário / Especial Publicitário - Órama",
            "parentCategory": "97251e49-4bd5-4eb7-b7c0-2190a08d8a26",
            "createdBy": "herman@corp.globo.com",
            "hostname": "g1.globo.com",
            "ad_unit": "tvg_G1/Economia/Seu_Dinheiro/Especial_Exclusivo",
            "disabled": False,
            "parents": [
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/3fced62b-25c5-41bb-9b24-c8c357e5e5a3",
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/cf9c1f0c-f737-4dbb-843c-4b2af64be9e1",
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/1e801343-a5d1-4d1b-b214-0a40852dbd47",
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/97251e49-4bd5-4eb7-b7c0-2190a08d8a26"
            ],
            "name": "Especial Publicitário - Órama",
            "ancestors": [
                "3fced62b-25c5-41bb-9b24-c8c357e5e5a3",
                "cf9c1f0c-f737-4dbb-843c-4b2af64be9e1",
                "1e801343-a5d1-4d1b-b214-0a40852dbd47",
                "97251e49-4bd5-4eb7-b7c0-2190a08d8a26"
            ],
            "path": "economia/educacao-financeira/especial-publicitario/orama",
            "usingPath": True,
            "folder": "https://g1-core.backstage.globoi.com/api/editorias/4620",
            "created": "2017-11-01T13:31:38.039Z",
            "id": "fcc541a5-0412-4a02-9313-e39935a73832",
            "versionId": "613039b6-ee97-450a-a84b-e849dcf7a234",
            "hierarchy_name": "G1 / Economia / Educação Financeira / Especial Publicitário / Especial Publicitário - Órama",
            "comercial_id": "1478af5f-aee9-49fd-9afc-501a7b8a1c74"
        },
        {
            "slug": "etapa-vestibular-2018",
            "modified": "2017-11-16T18:06:31.433Z",
            "parent": "a673bc00-4634-40ee-b311-f348282a100c",
            "ad_custom_data": "tvg_pgStr=g1/educação/especial publicitário/especial publicitário - etapa/especial publicitário - etapa vestibular 2018",
            "tenantId": "g1",
            "structure_tree": "G1 / Educação / Especial Publicitário / Especial Publicitário - Etapa / Especial Publicitário - Etapa Vestibular 2018",
            "parentCategory": "a673bc00-4634-40ee-b311-f348282a100c",
            "createdBy": "herman@corp.globo.com",
            "hostname": "g1.globo.com",
            "ad_unit": "tvg_G1/Educacao/Especial_Exclusivo",
            "disabled": False,
            "parents": [
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/3fced62b-25c5-41bb-9b24-c8c357e5e5a3",
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/f3c697fa-b583-4d17-b745-e7bc1bb8af5b",
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/e2618939-d898-4b46-8f67-2e961f5a392f",
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/a673bc00-4634-40ee-b311-f348282a100c"
            ],
            "name": "Especial Publicitário - Etapa Vestibular 2018",
            "ancestors": [
                "3fced62b-25c5-41bb-9b24-c8c357e5e5a3",
                "f3c697fa-b583-4d17-b745-e7bc1bb8af5b",
                "e2618939-d898-4b46-8f67-2e961f5a392f",
                "a673bc00-4634-40ee-b311-f348282a100c"
            ],
            "path": "educacao/especial-publicitario/etapa/etapa-vestibular-2018",
            "usingPath": True,
            "folder": "https://g1-core.backstage.globoi.com/api/editorias/4906",
            "created": "2017-10-30T19:48:29.943Z",
            "id": "f58fc351-d849-4591-8d1a-4430bd774e7d",
            "versionId": "3bce7880-ea18-433d-904c-022fde5abbcf",
            "hierarchy_name": "G1 / Educação / Especial Publicitário / Especial Publicitário - Etapa / Especial Publicitário - Etapa Vestibular 2018",
            "comercial_id": "621deaa8-afbb-4dce-9a7b-eee3c9a11b3d"
        },
        {
            "slug": "etapa",
            "modified": "2017-11-16T18:06:29.871Z",
            "parent": "e2618939-d898-4b46-8f67-2e961f5a392f",
            "ad_custom_data": "tvg_pgStr=g1/educação/especial publicitário/especial publicitário - etapa",
            "tenantId": "g1",
            "structure_tree": "G1 / Educação / Especial Publicitário / Especial Publicitário - Etapa",
            "parentCategory": "e2618939-d898-4b46-8f67-2e961f5a392f",
            "createdBy": "herman@corp.globo.com",
            "hostname": "g1.globo.com",
            "ad_unit": "tvg_G1/Educacao/Especial_Exclusivo",
            "disabled": False,
            "parents": [
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/3fced62b-25c5-41bb-9b24-c8c357e5e5a3",
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/f3c697fa-b583-4d17-b745-e7bc1bb8af5b",
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/e2618939-d898-4b46-8f67-2e961f5a392f"
            ],
            "name": "Especial Publicitário - Etapa",
            "ancestors": [
                "3fced62b-25c5-41bb-9b24-c8c357e5e5a3",
                "f3c697fa-b583-4d17-b745-e7bc1bb8af5b",
                "e2618939-d898-4b46-8f67-2e961f5a392f"
            ],
            "path": "educacao/especial-publicitario/etapa",
            "usingPath": True,
            "folder": "https://g1-core.backstage.globoi.com/api/editorias/3845",
            "created": "2017-10-30T19:48:25.146Z",
            "id": "a673bc00-4634-40ee-b311-f348282a100c",
            "versionId": "f7a6ba24-5ccf-41e4-ab2a-287d5c3ff7f5",
            "hierarchy_name": "G1 / Educação / Especial Publicitário / Especial Publicitário - Etapa",
            "comercial_id": "5b247237-e756-4f21-81a9-b394ae2c25e0"
        },
        {
            "slug": "especial-publicitario",
            "modified": "2017-11-16T18:06:28.390Z",
            "parent": "f3c697fa-b583-4d17-b745-e7bc1bb8af5b",
            "ad_custom_data": "tvg_pgStr=g1/educação/especial publicitário",
            "tenantId": "g1",
            "structure_tree": "G1 / Educação / Especial Publicitário",
            "parentCategory": "f3c697fa-b583-4d17-b745-e7bc1bb8af5b",
            "createdBy": "herman@corp.globo.com",
            "hostname": "g1.globo.com",
            "ad_unit": "tvg_G1/Educacao/Especial_Exclusivo",
            "disabled": False,
            "parents": [
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/3fced62b-25c5-41bb-9b24-c8c357e5e5a3",
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/f3c697fa-b583-4d17-b745-e7bc1bb8af5b"
            ],
            "name": "Especial Publicitário",
            "ancestors": [
                "3fced62b-25c5-41bb-9b24-c8c357e5e5a3",
                "f3c697fa-b583-4d17-b745-e7bc1bb8af5b"
            ],
            "path": "educacao/especial-publicitario",
            "usingPath": True,
            "folder": "https://g1-core.backstage.globoi.com/api/editorias/1855",
            "created": "2017-10-30T19:48:20.462Z",
            "id": "e2618939-d898-4b46-8f67-2e961f5a392f",
            "versionId": "b28e6a0e-42c5-4b41-9058-71fd56c29ede",
            "hierarchy_name": "G1 / Educação / Especial Publicitário",
            "comercial_id": "f0ba0b5d-3c2b-411d-90e5-07d6f30bfce8"
        },
        {
            "slug": "2017",
            "modified": "2017-11-16T18:06:02.008Z",
            "parent": "9c6d26ba-5d0a-409f-bacd-96be62216b14",
            "ad_custom_data": "tvg_pgStr=g1/tecnologia e games/hackathon/hackathon 2017",
            "tenantId": "g1",
            "structure_tree": "G1 / Economia / Tecnologia / Hackathon / Hackathon 2017",
            "parentCategory": "9c6d26ba-5d0a-409f-bacd-96be62216b14",
            "createdBy": "herman@corp.globo.com",
            "hostname": "g1.globo.com",
            "ad_unit": "tvg_G1/Tecnologia",
            "disabled": False,
            "parents": [
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/3fced62b-25c5-41bb-9b24-c8c357e5e5a3",
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/cf9c1f0c-f737-4dbb-843c-4b2af64be9e1",
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/cfb0a319-9951-40b9-beeb-6dbeb5a4f4d3",
                "https://apis.backstage.globoi.com/api/v2/backstage-structure/g1/9c6d26ba-5d0a-409f-bacd-96be62216b14"
            ],
            "name": "Hackathon 2017",
            "ancestors": [
                "3fced62b-25c5-41bb-9b24-c8c357e5e5a3",
                "cf9c1f0c-f737-4dbb-843c-4b2af64be9e1",
                "cfb0a319-9951-40b9-beeb-6dbeb5a4f4d3",
                "9c6d26ba-5d0a-409f-bacd-96be62216b14"
            ],
            "path": "economia/tecnologia/hackathon/2017",
            "usingPath": True,
            "folder": "https://g1-core.backstage.globoi.com/api/editorias/4442",
            "created": "2017-11-01T17:19:09.319Z",
            "id": "fd7a0c6c-9421-43cb-954b-83c68eae3556",
            "versionId": "3cc618dd-5a44-42bb-bd18-4f8f149c9561",
            "hierarchy_name": "G1 / Economia / Tecnologia / Hackathon / Hackathon 2017",
            "comercial_id": "c58fbc56-eb1a-4ab0-bd8b-0816749506d8"
        }
    ]
}

# for t in processed_values:
#     unmatched_structure_tree = processed_values[t].get('unmatched_structure_tree')
#     if unmatched_structure_tree:
#         for item in unmatched_structure_tree:
#             print(item['comercial_id'])

# MailSender(processed_values).send_mail()

message = 'teste'
MailSender(mail_client(), environ['CMA_WORKER_REPORT_SENDER'],
            environ['CMA_WORKER_REPORT_RECIPIENTS'], message).send_email()